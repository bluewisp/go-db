package sonicdb

import (
	"database/sql"
	"fmt"

	bundle "bitbucket.org/bluewisp/go-bundle"

	// used by the sql connector
	_ "github.com/go-sql-driver/mysql"
)

// DbSecrets holds data for the connection to the database
type DbSecrets map[string]interface{}

// Format the DbSecrets into a SQL readable connection string
func (dbs DbSecrets) Format() string {
	return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=%s", dbs["user"], dbs["pass"], dbs["host"], dbs["port"], dbs["name"], dbs["charset"])
}

// Open a new connection to the database
func Open(dbSecrets DbSecrets) (*sql.DB, error) {
	config := bundle.GetBundle("config")
	if config == nil {
		return nil, fmt.Errorf("configuration bundle not found")
	}

	db, err := sql.Open(config.GetString("database.driver"), dbSecrets.Format())
	if err != nil {
		return nil, err
	}

	return db, nil
}

// doSelectView rows from the database without binding it to a struct
func doSelectView(db *sql.DB, query string) ([]interface{}, error) {
	rows, err := selectRows(db, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	return dataMap(rows)
}

// doSelect rows from the database
func doSelect(db *sql.DB, query string, i interface{}) ([]interface{}, error) {
	rows, err := selectRows(db, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	return dataBind(rows, i)
}

// doInsert data into the db
func doInsert(db *sql.DB, i interface{}) (int64, error) {
	// Create insert query from interface
	query := createInsertQuery(i)

	// Execute the query
	res, err := db.Exec(query)
	if err != nil {
		return sql.NullInt64{}.Int64, err
	}

	// Get the last inserted ID
	id, err := res.LastInsertId()
	if err != nil {
		return sql.NullInt64{}.Int64, err
	}

	// Return the ID
	return id, nil
}

// doInsertAll ...
func doInsertAll(db *sql.DB, i ...interface{}) []int64 {
	ids := make([]int64, 0)

	for _, obj := range i {
		if id, err := doInsert(db, obj); err == nil {
			ids = append(ids, id)
		}
	}

	return ids
}

// doUpdate data into the db
func doUpdate(db *sql.DB, query string) (int64, error) {
	// Execute the query
	res, err := db.Exec(query)
	if err != nil {
		return sql.NullInt64{}.Int64, err
	}

	return res.RowsAffected()
}

// doDelete data in the database
func doDelete(db *sql.DB, query string) (int64, error) {
	// Execute the query
	res, err := db.Exec(query)
	if err != nil {
		return sql.NullInt64{}.Int64, err
	}

	return res.RowsAffected()
}

func selectRows(db *sql.DB, query string) (*sql.Rows, error) {
	// Execute the query
	rows, err := db.Query(query)
	if err != nil {
		return nil, err
	}

	return rows, nil
}

func dataMap(rows *sql.Rows) ([]interface{}, error) {
	// Create an empty slice of interfaces
	objects := make([]interface{}, 0)

	for rows.Next() {
		object, err := Extract(rows)
		if err != nil {
			return objects, err
		}

		objects = append(objects, object)
	}

	return objects, nil
}

func dataBind(rows *sql.Rows, i interface{}) ([]interface{}, error) {
	// Create an empty slice of interfaces
	objects := make([]interface{}, 0)

	// Loop over the results from the database
	for rows.Next() {
		object, err := Bind(i, rows)
		if err != nil {
			return objects, err
		}

		objects = append(objects, object)
	}

	return objects, nil
}
