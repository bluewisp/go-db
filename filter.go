package sonicdb

// Filters is a type reference to a filter map
type Filters map[string]interface{}

// Filter a data set based on a map from a request
func Filter(filters Filters) []Clause {
	clauses := make([]Clause, 0)

	for filter, value := range filters {
		switch filter {
		case "equals":
			clauses = append(clauses, Equals(filter, value))
			break
		}
	}

	return Criteria(clauses...)
}
