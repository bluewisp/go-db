package sonicdb

import (
	"fmt"
	"reflect"
	"strings"
)

// InsertStmt ...
const InsertStmt = "INSERT INTO"

// SelectStmt ...
const SelectStmt = "SELECT"

// UpdateStmt ...
const UpdateStmt = "UPDATE"

// DeleteStmt ...
const DeleteStmt = "DELETE FROM"

type set struct {
	key         string
	placeholder string
}

// Query ...
type Query struct {
	method       string
	expression   string
	caseWrappers []*CaseWrapper
	tableName    string
	alias        string
	set          []set
	fields       []*Field
	joins        []join
	clauses      []On
	groupBy      []string
	orderBy      []orderBy
	offset       int64
	limit        int64
}

// NewQuery returns a new query
func NewQuery(method string) *Query {
	return &Query{method: method}
}

// getSQL builds the SQL statement from the query struct
func (q *Query) getSQL() string {
	var sections []string

	sections = append(sections, q.method)

	switch q.method {
	case SelectStmt:
		// Add expression
		sections = append(sections, buildSelectExpr(q))
		// Add from
		sections = append(sections, buildMethodExpr(q.tableName, q.alias, q.method))
		// Joins
		sections = append(sections, buildJoins(q.joins))
		// Resolve where clause
		if len(q.clauses) > 0 {
			sections = append(sections, "WHERE "+buildWhereClauses(q.clauses))
		}
		if len(q.groupBy) > 0 {
			sections = append(sections, "GROUP BY "+buildGroupBy(q.groupBy))
		}
		if len(q.orderBy) > 0 {
			sections = append(sections, "ORDER BY "+buildOrderBys(q.orderBy))
		}
		if q.limit != 0 {
			sections = append(sections, fmt.Sprintf("LIMIT %d", q.limit))
		}
		if q.offset != 0 {
			sections = append(sections, fmt.Sprintf("OFFSET %d", q.offset))
		}
	case InsertStmt:
		// Add from
		sections = append(sections, buildMethodExpr(q.tableName, q.alias, q.method))
		// Add columns and values associated with the columns
		sections = append(sections, buildInsertExpr(q.fields))
	case UpdateStmt:
		// Add from
		sections = append(sections, buildMethodExpr(q.tableName, q.alias, q.method))
		// Resolve where clause
		sections = append(sections, "SET "+buildUpdateExpr(q.fields))
		// Resolve where clause
		if len(q.clauses) > 0 {
			sections = append(sections, "WHERE "+buildWhereClauses(q.clauses))
		}
	case DeleteStmt:
		// Add from
		sections = append(sections, buildMethodExpr(q.tableName, q.alias, q.method))
		// Resolve where clause
		if len(q.clauses) > 0 {
			sections = append(sections, "WHERE "+buildWhereClauses(q.clauses))
		}
	}

	stmt := strings.Join(sections, " ")

	return stmt
}

// createInsertQuery ...
func createInsertQuery(i interface{}) string {
	query := NewQuery(InsertStmt)
	tableName := GetTableName(i)
	query.setTableName(tableName)
	query.setFields(i)

	return query.getSQL()
}

// setFields takes columns and values from struct and converts it to a insert query
func (q *Query) setFields(i interface{}) *Query {
	// Create a value reflection of the struct that has been passed
	v := reflect.ValueOf(i)
	if v.Kind() == reflect.Ptr {
		v = v.Elem()
	}

	// Create fields
	q.fields = getInterfaceFields(v, nil)

	return q
}

// AddSet Specially for UPDATE statements
func (q *Query) AddSet(key, placeholder string) *Query {
	s := set{key: key, placeholder: placeholder}
	q.set = append(q.set, s)

	return q
}

// AddLimit ...
func (q *Query) AddLimit(limit int64) *Query {
	if limit > 0 {
		q.limit = limit
	}

	return q
}

// AddOffset ...
func (q *Query) AddOffset(offset int64) *Query {
	if offset > 0 {
		q.offset = offset
	}

	return q
}
