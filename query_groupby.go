package sonicdb

// AddGroupBy ...
func (q *Query) AddGroupBy(column string) {
	q.groupBy = append(q.groupBy, column)
}
