package sonicdb

import (
	"encoding/json"
	"time"
)

// Time wrapper for golang's Time
type Time struct {
	time.Time
}

// Now returns the current time
func Now() Time {
	return Time{
		Time: time.Now().UTC(),
	}
}

// ZeroTime returns a zero time object, to clean datetime database columns
func ZeroTime() Time {
	return Time{
		Time: time.Time{},
	}
}

// CreateTime returns Time from string datetime
func CreateTime(value string) (Time, error) {
	t, err := time.Parse("2006-01-02 15:04:05", value)
	if err != nil {
		return Now(), err
	}
	return Time{
		Time: t,
	}, nil
}

// Sub returns the difference between 2 times
func (t Time) Sub(t2 Time) time.Duration {
	return t.Time.Sub(t2.Time)
}

// Localized returns localized time string in custom layout
func (t Time) Localized(layout string) string {
	// FIXME dynamic location
	loc, _ := time.LoadLocation("Europe/Amsterdam")

	return t.In(loc).Format(layout)
}

// MarshalJSON is a custom implementation for MarshalJSON
func (t Time) MarshalJSON() ([]byte, error) {
	if t.IsZero() {
		return json.Marshal(nil)
	}
	return json.Marshal(t.Time.UTC().Format("2006-01-02 15:04:05"))
}
