package sonicdb

import "testing"

var cases = map[string]string{
	"CamelCase":  "camel_case",
	"Camel%Case": "camel%_case",
	"ThisWillBe": "this_will_be",
}

func TestUnderscore(t *testing.T) {
	for value, expected := range cases {
		t.Run(value, func(t *testing.T) {
			if actual := Underscore(value); actual != expected {
				t.Error("expected", expected, "got", actual)
			}
		})
	}
}
